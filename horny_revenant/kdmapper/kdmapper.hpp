#pragma once
#include <Windows.h>
#include <stdint.h>
#include <iostream>
#include <vector>
#include <string>
#include <filesystem>

#include "portable_executable.hpp"
#include "utils.hpp"
#include "nt.hpp"
//#include "intel_driver.hpp"

#include "../WVDM.hpp"

namespace kdmapper
{
	uint64_t MapDriver(WVDM& wvdm, const std::string& driver_path);
	void RelocateImageByDelta(portable_executable::vec_relocs relocs, const uint64_t delta);
	bool ResolveImports(WVDM& wvdm, portable_executable::vec_imports imports);
}