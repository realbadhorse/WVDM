#include "VMPA.h"


#define PFN_TO_PA(pfn) (pfn << 12)
#define A_TO_PFN(pa) (pa >> 12)



u64 VMPA::vm_to_pa(u64 vm_) {
	virt_addr_t va = { .value = vm_ };

	pml4e_t pml4e;
	pdpte_t pdpte;
	pde_t pde;
	pte_t pte;

	wvdm.readmem(usermode_pml4_pa +		(sizeof(pml4e)	*	va.pml4_index), 
		&pml4e, 
		sizeof(pml4e_t));
	wvdm.readmem(PFN_TO_PA(pml4e.pfn) + (sizeof(pdpte_t) *	va.pdpt_index),
		&pdpte,
		sizeof(pdpte_t));
	wvdm.readmem(PFN_TO_PA(pdpte.pfn) + (sizeof(pde_t)	*	va.pd_index),
		&pde,
		sizeof(pde_t));
	wvdm.readmem(PFN_TO_PA(pde.pfn) +	(sizeof(pte_t)	*	va.pt_index),
		&pte,
		sizeof(pte_t));

	u64 physical_address = PFN_TO_PA(pte.pfn) + va.offset;
	return physical_address;
}

void VMPA::readvm(u64 vm_, void* outBuffer_, int size_){
	if (A_TO_PFN(vm_) != A_TO_PFN(vm_ + size_)) {
		LOG("Cross-page read not supproted\n");
	}
	//not tested yet
	u64 pa = vm_to_pa(vm_);
	wvdm.readmem(pa, outBuffer_, size_);
}

void VMPA::writevm(u64 vm_, void* outBuffer_, int size_){
	if (A_TO_PFN(vm_) != A_TO_PFN(vm_ + size_)) {
		LOG("Cross-page read not supproted\n");
	}
	//not tested yet
	u64 pa = vm_to_pa(vm_);
	wvdm.writemem(pa, outBuffer_, size_);
}


VMPA::VMPA(WVDM& wvdm_, u64 um_pml4_pa_) : wvdm(wvdm_)
{
	wvdm = wvdm_;
	usermode_pml4_pa = um_pml4_pa_;
}

void VMPA::test()
{
	for (int i = 0; i < 512; i++) {
		pml4e_t pml4e;
		wvdm.readmem(
			usermode_pml4_pa + (sizeof(pml4e_t) * i),
			&pml4e,
			sizeof(pml4e_t)
		);
		printf("[%d] present: %d\n", i, pml4e.present);
	}
}