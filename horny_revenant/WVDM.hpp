#pragma once
#include <Windows.h>

#include <vector>
#include <type_traits>
#include <map>
#include <assert.h>

#include "util.h"
#include "types.h"

#ifdef _DEBUG
#define LOG(...) std::printf(__VA_ARGS__)

#else
#define LOG(...) (0)
#endif // _NDEBUG




static void (*rwpm)(HANDLE fd_, u64 physical_address_, void* buffer, unsigned int size);

class WVDM {

private:
	//statics to increase perf
	u64 mmgetsystemroutine_kva = 0,
		getphysicaladdress_kva = 0;
	//
	
	//size -> pair(syscall PA, syscall num)
	std::map<int, std::pair<u64, int>> syscall_arg_map;

	//user supplied fn ptr
	void (*readpm)(HANDLE fd_, u64 physical_address_, void* buffer, unsigned int size) { 0 };
	void (*writepm)(HANDLE fd_, u64 physical_address_, void* buffer, unsigned int size) { 0 };
	//

	//syscall
	void* (*syscall_invoker)();
	bool init_syscall_invoker();
	void _set_syscall_id(unsigned __int32);
	//InvokeSyscall: template
	//

	//scanning and syscall hook setup
	bool scan_initial_syscalls(); //get initial 0-4 syscalls
	bool fill_syscalls(); //fill up map using mmgetsysroutineaddr and getphysicaladdr
	bool test_shutdown(u64 addr_);
	std::vector<u64> find_pattern_at_pageoffset(const char* pattern_, int len_, int page_offset_);
	//

	HANDLE fd = INVALID_HANDLE_VALUE;
	HANDLE get_driver(const WCHAR* DeviceName_);


public:
	//wrappers
	inline int copymemory(void* dst, void* src, u64 size);
	inline int comparememory(void* dst, void* src, u64 size);
	
	u64 allocatepool(u64 pool_type, u64 size);
	u64 freepool(u64 kva_);

	u64 allocatepages(u64 size);
	u64 setpageprotection(u64 address, u64 size, u64 protection);
	//

	//callback stuff
	auto get_code_pattern(const char* modulename_, const unsigned char* opcodes, const char* opcodes_mask, u64 opcodes_size)->u64;
	//

	bool init_success = false;
	WVDM(const WCHAR* devicename, decltype(rwpm) readpm_, decltype(rwpm) writepm_);

	// MUST be able to evaluate arbitrary 'size'
	void (readmem)(u64 physical_address_, void* buffer, unsigned int size);
	void (writemem)(u64 physical_address_, void* buffer, unsigned int size);

	void s_readmem(u64 physical_address_, void* buffer, unsigned int size);
	void s_writemem(u64 physical_address_, void* buffer, unsigned int size);

	u64 get_paging_base_pa(const WCHAR* procname);


	template <class T, class ... Ts>
	std::invoke_result_t<T, Ts...> InvokeSyscall(u64 syscall_number_, Ts ... args) {
		_set_syscall_id(syscall_number_);
		auto result = reinterpret_cast<T>(syscall_invoker)(args...);
		return result;
	}

	template <class T, class ... Ts>
	auto syscall(u64 routine_kva_, Ts ... args) -> std::invoke_result_t<T, Ts...>
	{
		assert(routine_kva_);
		const std::size_t arg_count = sizeof...(Ts);
		u64 appropriate_syscall_pa, appropriate_syscall_num;
		LOG("Calling routine at kva: 0x%p\n", routine_kva_);

		if (syscall_arg_map.count(arg_count) != 0) {
			std::tie(appropriate_syscall_pa, appropriate_syscall_num) = syscall_arg_map[arg_count];
		}
		else {
			LOG("Cannot call function with %d arguments\n", arg_count);
			return 0;
		}

		//jmp [rip+0]
		unsigned char jump_patch[] =
		{
				0xff, 0x25, 0x00, 0x00,
				0x00, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x00, 0x00,
				0x00, 0x00
		};
		char orig_bytes[sizeof(jump_patch)];

		*(u64*)(jump_patch + 6) = routine_kva_;

		//save orig
		readmem(appropriate_syscall_pa, orig_bytes, sizeof(orig_bytes));

		//place jump, call and restore
		writemem(appropriate_syscall_pa, jump_patch, sizeof(jump_patch));
		//LOG("Patched bytes\n");

		auto result = InvokeSyscall<T>(appropriate_syscall_num, args...);

		writemem(appropriate_syscall_pa, orig_bytes, sizeof(orig_bytes));
		//LOG("Finished syscall\n");

		return result;
	}

	template <class T, class ... Ts>
	std::invoke_result_t<T, Ts...> syscall(const char* system_routine_name_, Ts ... args) {

		u64 desired_routine_kva =
			(u64)Util::get_kmodule_export("ntoskrnl.exe", system_routine_name_);
		if (!desired_routine_kva) {
			LOG("COULD NOT FIND ROUTINE\n");
			std::terminate();
		}

		auto result = syscall<T>(desired_routine_kva, args...);

		return result;

	}


};