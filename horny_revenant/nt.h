#pragma once

#include <Windows.h>


#pragma comment(lib, "ntdll.lib")
#define PAGE_4KB 0x1000

constexpr auto SystemModuleInformation = 11;
typedef struct _RTL_PROCESS_MODULE_INFORMATION
{
    HANDLE Section;
    PVOID MappedBase;
    PVOID ImageBase;
    ULONG ImageSize;
    ULONG Flags;
    USHORT LoadOrderIndex;
    USHORT InitOrderIndex;
    USHORT LoadCount;
    USHORT OffsetToFileName;
    CHAR FullPathName[256];
} RTL_PROCESS_MODULE_INFORMATION, * PRTL_PROCESS_MODULE_INFORMATION;

typedef struct _RTL_PROCESS_MODULES
{
    ULONG NumberOfModules;
    RTL_PROCESS_MODULE_INFORMATION Modules[1];
} RTL_PROCESS_MODULES, * PRTL_PROCESS_MODULES;

typedef LARGE_INTEGER PHYSICAL_ADDRESS, * PPHYSICAL_ADDRESS;

using PEPROCESS = PVOID;
using PsLookupProcessByProcessId = NTSTATUS(__fastcall*)(
    HANDLE     ProcessId,
    PEPROCESS* Process
    );

namespace Callbacks {
    typedef struct _OBJECT_TYPE* POBJECT_TYPE;
    typedef ULONG OB_OPERATION;

    //
    // Registration version for Vista SP1 and Windows Server 2007
    //
#define OB_FLT_REGISTRATION_VERSION_0100  0x0100

//
// This value should be used by filters for registration
//
#define OB_FLT_REGISTRATION_VERSION OB_FLT_REGISTRATION_VERSION_0100

    typedef ULONG OB_OPERATION;

#define OB_OPERATION_HANDLE_CREATE              0x00000001
#define OB_OPERATION_HANDLE_DUPLICATE           0x00000002

    typedef struct _OB_PRE_CREATE_HANDLE_INFORMATION {
        _Inout_ ACCESS_MASK         DesiredAccess;
        _In_ ACCESS_MASK            OriginalDesiredAccess;
    } OB_PRE_CREATE_HANDLE_INFORMATION, * POB_PRE_CREATE_HANDLE_INFORMATION;

    typedef struct _OB_PRE_DUPLICATE_HANDLE_INFORMATION {
        _Inout_ ACCESS_MASK         DesiredAccess;
        _In_ ACCESS_MASK            OriginalDesiredAccess;
        _In_ PVOID                  SourceProcess;
        _In_ PVOID                  TargetProcess;
    } OB_PRE_DUPLICATE_HANDLE_INFORMATION, * POB_PRE_DUPLICATE_HANDLE_INFORMATION;

    typedef union _OB_PRE_OPERATION_PARAMETERS {
        _Inout_ OB_PRE_CREATE_HANDLE_INFORMATION        CreateHandleInformation;
        _Inout_ OB_PRE_DUPLICATE_HANDLE_INFORMATION     DuplicateHandleInformation;
    } OB_PRE_OPERATION_PARAMETERS, * POB_PRE_OPERATION_PARAMETERS;

    typedef struct _OB_PRE_OPERATION_INFORMATION {
        _In_ OB_OPERATION           Operation;
        union {
            _In_ ULONG Flags;
            struct {
                _In_ ULONG KernelHandle : 1;
                _In_ ULONG Reserved : 31;
            };
        };
        _In_ PVOID                         Object;
        _In_ POBJECT_TYPE                  ObjectType;
        _Out_ PVOID                        CallContext;
        _In_ POB_PRE_OPERATION_PARAMETERS  Parameters;
    } OB_PRE_OPERATION_INFORMATION, * POB_PRE_OPERATION_INFORMATION;

    typedef struct _OB_POST_CREATE_HANDLE_INFORMATION {
        _In_ ACCESS_MASK            GrantedAccess;
    } OB_POST_CREATE_HANDLE_INFORMATION, * POB_POST_CREATE_HANDLE_INFORMATION;

    typedef struct _OB_POST_DUPLICATE_HANDLE_INFORMATION {
        _In_ ACCESS_MASK            GrantedAccess;
    } OB_POST_DUPLICATE_HANDLE_INFORMATION, * POB_POST_DUPLICATE_HANDLE_INFORMATION;

    typedef union _OB_POST_OPERATION_PARAMETERS {
        _In_ OB_POST_CREATE_HANDLE_INFORMATION       CreateHandleInformation;
        _In_ OB_POST_DUPLICATE_HANDLE_INFORMATION    DuplicateHandleInformation;
    } OB_POST_OPERATION_PARAMETERS, * POB_POST_OPERATION_PARAMETERS;

    typedef struct _OB_POST_OPERATION_INFORMATION {
        _In_ OB_OPERATION  Operation;
        union {
            _In_ ULONG Flags;
            struct {
                _In_ ULONG KernelHandle : 1;
                _In_ ULONG Reserved : 31;
            };
        };
        _In_ PVOID                          Object;
        _In_ POBJECT_TYPE                   ObjectType;
        _In_ PVOID                          CallContext;
        _In_ NTSTATUS                       ReturnStatus;
        _In_ POB_POST_OPERATION_PARAMETERS  Parameters;
    } OB_POST_OPERATION_INFORMATION, * POB_POST_OPERATION_INFORMATION;

    typedef enum _OB_PREOP_CALLBACK_STATUS {
        OB_PREOP_SUCCESS
    } OB_PREOP_CALLBACK_STATUS, * POB_PREOP_CALLBACK_STATUS;

    typedef OB_PREOP_CALLBACK_STATUS
    (*POB_PRE_OPERATION_CALLBACK) (
        _In_ PVOID RegistrationContext,
        _Inout_ POB_PRE_OPERATION_INFORMATION OperationInformation
        );

    typedef VOID
    (*POB_POST_OPERATION_CALLBACK) (
        _In_ PVOID RegistrationContext,
        _In_ POB_POST_OPERATION_INFORMATION OperationInformation
        );

    typedef struct _OB_OPERATION_REGISTRATION {
        _In_ POBJECT_TYPE* ObjectType;
        _In_ OB_OPERATION                Operations;
        _In_ POB_PRE_OPERATION_CALLBACK  PreOperation;
        _In_ POB_POST_OPERATION_CALLBACK PostOperation;
    } OB_OPERATION_REGISTRATION, * POB_OPERATION_REGISTRATION;

    typedef struct _OB_CALLBACK_REGISTRATION {
        _In_ USHORT                     Version;
        _In_ USHORT                     OperationRegistrationCount;
        _In_ UNICODE_STRING             Altitude;
        _In_ PVOID                      RegistrationContext;
        _In_ OB_OPERATION_REGISTRATION* OperationRegistration;
    } OB_CALLBACK_REGISTRATION, * POB_CALLBACK_REGISTRATION;


}
