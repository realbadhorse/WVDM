#pragma once

#include <vector>
#include "types.h"

namespace Util {
	u64 get_syscallnum_from_ntdll_export(const char* export_);
	auto get_kmodule_export(const char* module_name, const char* export_name, bool rva = false) -> void*;
	auto get_loaded_kernel_base_kva(const char* module_name) -> u64;
	std::vector<std::pair<u64, u64>> get_memory_ranges();
	PVOID pattern_scan(char* haystack_, int haystack_size_, const char* needle_, int needle_size_, const char * needle_mask_);
	int get_pid(const WCHAR* procname);
}