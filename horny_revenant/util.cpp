#include <windows.h>
#include <tlhelp32.h>
#include <winternl.h>
//#include <ntstatus.h>
#define STATUS_INFO_LENGTH_MISMATCH 0xC0000004

#include <string>
#include <cstdint>
#include <string_view>
#include <algorithm>
#include <string_view>
#include <map>

//todo: port to pe_image
#include "nt.h"
#include "types.h"

#include "regmem.h"

const u64 GB_1 = 0x40000000;

namespace Util {

// this prints out all the memory maps from the different keys
//https://github.com/LordNoteworthy/al-khaser/issues/112
//https://github.com/LordNoteworthy/al-khaser/files/2045737/PhysMemResourceList.c.txt
	std::vector<std::pair<u64, u64>> get_memory_ranges()
	{
		DWORD count;
		std::vector<std::pair<u64, u64>> vec_ranges;

		//printf("[*] Getting physical memory regions from registry\n");
		struct memory_region* regions = (struct memory_region*)malloc(sizeof(struct memory_region) * 10);


		count = parse_memory_map(regions, ResourceRegistryKeys[0]);

		for (DWORD i = 0; i < count; i++) {

			vec_ranges.push_back(std::make_pair(regions[i].address, regions[i].address + regions[i].size));

			//printf("  --> Memory region found: %.16llx - %.16llx\n", regions[i].address, regions[i].address + regions[i].size);
		}
		return vec_ranges;
	}

	u64 get_syscallnum_from_ntdll_export(const char* export_) {
		u64 export_address = (u64)GetProcAddress(LoadLibraryA("ntdll.dll"), export_);
		if (!export_address) return 0;
		auto result = *(__int32*)(export_address + 4);
		return result;
	}

	//https://githacks.org/_xeroxz/vdm/-/blob/master/VDM/util/util.hpp#L131
	auto get_kmodule_export(const char* module_name, const char* export_name, bool rva = false) -> void*
	{
		constexpr auto SystemModuleInformation = 11;
		void* buffer = nullptr;
		DWORD buffer_size = NULL;

		NTSTATUS status = NtQuerySystemInformation(
			static_cast<SYSTEM_INFORMATION_CLASS>(SystemModuleInformation),
			buffer,
			buffer_size,
			&buffer_size
		);

		while (status == STATUS_INFO_LENGTH_MISMATCH)
		{
			VirtualFree(buffer, 0, MEM_RELEASE);
			buffer = VirtualAlloc(nullptr, buffer_size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
			status = NtQuerySystemInformation(
				static_cast<SYSTEM_INFORMATION_CLASS>(SystemModuleInformation),
				buffer,
				buffer_size,
				&buffer_size
			);
		}

		if (!NT_SUCCESS(status))
		{
			VirtualFree(buffer, 0, MEM_RELEASE);
			return nullptr;
		}

		const auto modules = static_cast<PRTL_PROCESS_MODULES>(buffer);
		for (auto idx = 0u; idx < modules->NumberOfModules; ++idx)
		{
			// find module and then load library it
			const std::string current_module_name =
				std::string(reinterpret_cast<char*>(
					modules->Modules[idx].FullPathName) +
					modules->Modules[idx].OffsetToFileName
				);

			if (!_stricmp(current_module_name.c_str(), module_name))
			{
				std::string full_path = reinterpret_cast<char*>(modules->Modules[idx].FullPathName);
				full_path.replace(full_path.find("\\SystemRoot\\"),
					sizeof("\\SystemRoot\\") - 1, std::string(getenv("SYSTEMROOT")).append("\\"));

				const auto module_base =
					LoadLibraryExA(
						full_path.c_str(),
						NULL,
						DONT_RESOLVE_DLL_REFERENCES
					);

				PIMAGE_DOS_HEADER p_idh;
				PIMAGE_NT_HEADERS p_inh;
				PIMAGE_EXPORT_DIRECTORY p_ied;

				PDWORD addr, name;
				PWORD ordinal;

				p_idh = (PIMAGE_DOS_HEADER)module_base;
				if (p_idh->e_magic != IMAGE_DOS_SIGNATURE)
					return NULL;

				p_inh = (PIMAGE_NT_HEADERS)((LPBYTE)module_base + p_idh->e_lfanew);
				if (p_inh->Signature != IMAGE_NT_SIGNATURE)
					return NULL;

				if (p_inh->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress == 0)
					return NULL;

				p_ied = (PIMAGE_EXPORT_DIRECTORY)((LPBYTE)module_base +
					p_inh->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);

				addr = (PDWORD)((LPBYTE)module_base + p_ied->AddressOfFunctions);
				name = (PDWORD)((LPBYTE)module_base + p_ied->AddressOfNames);
				ordinal = (PWORD)((LPBYTE)module_base + p_ied->AddressOfNameOrdinals);

				// find exported function
				for (unsigned int i = 0; i < p_ied->AddressOfFunctions; i++)
				{
					if (!strcmp(export_name, (char*)module_base + name[i]))
					{
						if (!rva)
						{
							auto result = (void*)((std::uintptr_t)modules->Modules[idx].ImageBase + addr[ordinal[i]]);
							VirtualFree(buffer, NULL, MEM_RELEASE);
							return result;
						}
						else
						{
							auto result = (void*)addr[ordinal[i]];
							VirtualFree(buffer, NULL, MEM_RELEASE);
							return result;
						}
					}
				}
			}
		}

		VirtualFree(buffer, NULL, MEM_RELEASE);
		return nullptr;
	}

	//http://www.rohitab.com/discuss/topic/40696-list-loaded-drivers-with-ntquerysysteminformation/
	auto get_loaded_kernel_base_kva(const char* module_name_) -> u64 {
		NTSTATUS status;
		ULONG i;

		PRTL_PROCESS_MODULES ModuleInfo;

		ModuleInfo = (PRTL_PROCESS_MODULES)VirtualAlloc(NULL, 1024 * 1024, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE); // Allocate memory for the module list

		if (!ModuleInfo)
		{
			//printf("\nUnable to allocate memory for module list (%d)\n", GetLastError());
			return -1;
		}

		if (!NT_SUCCESS(status = NtQuerySystemInformation((SYSTEM_INFORMATION_CLASS)11, ModuleInfo, 1024 * 1024, NULL))) // 11 = SystemModuleInformation
		{
			//printf("\nError: Unable to query module list (%#x)\n", status);
			VirtualFree(ModuleInfo, 0, MEM_RELEASE);
			return -1;
		}

		for (i = 0; i < ModuleInfo->NumberOfModules; i++)
		{

			if (
				std::string(ModuleInfo->Modules[i].FullPathName).ends_with(module_name_)
				) {
				return (u64)ModuleInfo->Modules[i].ImageBase;
			}

			//printf("\nImage base: %#x\n", ModuleInfo->Modules[i].ImageBase);
			//printf("\nImage name: %s\n", ModuleInfo->Modules[i].FullPathName + ModuleInfo->Modules[i].OffsetToFileName);
			//printf("\nImage full path: %s\n", ModuleInfo->Modules[i].FullPathName);
			//printf("\nImage size: %d\n", ModuleInfo->Modules[i].ImageSize);
		}

		VirtualFree(ModuleInfo, 0, MEM_RELEASE);
		return 0;
	}

	PVOID pattern_scan(char* haystack_, int haystack_size_, const char* needle_, int needle_size_, const char * needle_mask_) {
		char* haystack = haystack_;
		while (haystack + needle_size_ < haystack_ + haystack_size_) {
			for (int i = 0; i < needle_size_; i++) {
				//LOG("[%d:%d - %d %d]\n", haystack[i], needle_[i], i, needle_size_);
				if (needle_mask_[i] == 'x') {
					if (haystack[i] != needle_[i])
						break;
					if (i == (needle_size_ - 1)) // one for zero-indexing
						return haystack;
				}
			}
			haystack++;
		}
		return 0;
	}

	int get_pid(const WCHAR* procname) {
		PROCESSENTRY32 entry;
		entry.dwSize = sizeof(PROCESSENTRY32);
		int pid = 0;

		HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

		if (Process32First(snapshot, &entry) == TRUE)
		{
			while (Process32Next(snapshot, &entry) == TRUE)
			{
				if (_wcsicmp(entry.szExeFile, procname) == 0)
				{
					HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);

					pid = entry.th32ProcessID;

					CloseHandle(hProcess);
				}
			}
		}
		return pid;
	}
}